// File author is Ítalo Lima Marconato Matias
//
// Created on December 09 of 2018, at 19:27 BRT
// Last edited on October 27 of 2019, at 16:13 BRT

#include <chicago/types.h>

PWChar NlsMessagesBr[] = {
	L"Desculpe, o CHicago encontrou um erro fatal e não pode continuar operando.\r\nVocê terá que reiniciar seu computador manualmente.\r\n\r\n",
	
	L"\r\nCódigo do Erro: %s\r\n",
	
	L"Sistema Operacional CHicago para %s\r\n",
	
	L"Codinome '%s'\r\n",
	
	L"Versão %d.%d.%d\r\n\r\n",
	
	L"Falha de segmentação\r\n",
};
