// File author is Ítalo Lima Marconato Matias
//
// Created on December 09 of 2018, at 19:27 BRT
// Last edited on October 27 of 2019, at 16:14 BRT

#include <chicago/types.h>

PWChar NlsMessagesEn[] = {
	L"Sorry, CHicago got a fatal error and can't continue operating.\r\nYou need to restart your computer manually.\r\n\r\n",
	
	L"\r\nError Code: %s\r\n",
	
	L"CHicago Operating System for %s\r\n",
	
	L"Codename '%s'\r\n",
	
	L"Version %d.%d.%d\r\n\r\n",
	
	L"Segmentation fault\r\n",
};
